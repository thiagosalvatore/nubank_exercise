#Account schema

# --- !Ups

CREATE TABLE Accounts (  
    id SERIAL PRIMARY KEY,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL
);

INSERT INTO Accounts (first_name, last_name) Values('Thiago', 'Salvatore');    

# --- !Downs

DROP TABLE Accounts;