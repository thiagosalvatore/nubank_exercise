# Operations schema

# --- !Ups

CREATE TABLE Operations (  
    id SERIAL PRIMARY KEY,
    account_id SERIAL NOT NULL REFERENCES accounts (id) ON DELETE CASCADE,
    amount integer NOT NULL,
    kind varchar(100) NOT NULL,
    description varchar(255) NOT NULL,
    date date NOT NULL
);

INSERT INTO Operations (account_id, description, amount, kind, date) Values(1, 'Testando1', 232, 'deposit', '2017-03-15');  
INSERT INTO Operations (account_id, description, amount, kind, date) Values(1, 'Testando2', -110, 'purchase', '2017-03-17');  
INSERT INTO Operations (account_id, description, amount, kind, date) Values(1, 'Testando3', -150, 'withdraw', '2017-03-21');  

# --- !Downs

DROP TABLE Operations; 