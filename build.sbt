name := """nubank-exercise"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

sources in (Compile, doc) := Seq.empty

// Compile the project before generating Eclipse files, so that generated .scala or .class files for views and routes are present
EclipseKeys.preTasks := Seq(compile in Compile)

publishArtifact in (Compile, packageDoc) := false

libraryDependencies += cache
libraryDependencies += ws
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1204-jdbc4"
libraryDependencies += "com.typesafe.play" %% "play-slick" % "1.1.0"
libraryDependencies += "com.typesafe.play" %% "play-slick-evolutions" % "1.1.0"