package controllers

import javax.inject._
import play.api._
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global  
import scala.concurrent.Future

import org.postgresql.util.PSQLException

import org.joda.time.DateTime
import java.sql.Date

import models.Account
import models.Accounts
import dao.AccountsDAOTrait

import services.OperationService

/*
  This controller handles requests related to account (creation, list and deletion)
 */
@Singleton
class AccountController @Inject()(service: OperationService, accountDAO: AccountsDAOTrait) extends Controller {

  //List all accounts in the database
  def index = Action.async {
     val result = accountDAO.retrieveAllAccounts()
     result.map(ops => Ok(Json.obj("status" -> "Ok", "accounts" -> Json.toJson(ops))))
  }
  
  //Creates a new account
  def create = Action.async(BodyParsers.parse.json) { request =>
    val account = request.body.validate[Account]
    
    account.fold(
      errors => Future(BadRequest(Json.obj(
            "status" -> "Parsing account failed",
            "error" -> JsError.toJson(errors)))),
      account =>
         accountDAO.createAccount(account).map(m => Ok(Json.obj("status" -> "Success", "account" -> Json.toJson(m)))) 
     ) 
  }
  
  //Deletes an account
  def delete(id: Int) = Action.async {
    val result = accountDAO.deleteAccount(id)
    result.map(ops => Ok(Json.obj("status" -> "Ok", "accounts" -> Json.toJson(ops))))  
  }
  
  
}