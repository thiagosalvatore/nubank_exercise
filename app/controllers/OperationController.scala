package controllers

import javax.inject._
import play.api._
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global  
import scala.concurrent.Future

import org.postgresql.util.PSQLException

import org.joda.time.DateTime
import java.sql.Date

import models.Operation
import models.Operations
import dao.OperationsDAOTrait
import utils.DataHandler

import services.OperationTrait


/*
  This controller is responsible for dealing with requests related to transactions (statements, operations, periods of debits, and so on)
 */
@Singleton
class OperationController @Inject()(service: OperationTrait, operationDAO: OperationsDAOTrait, DataHandler: DataHandler) extends Controller {

  def index = Action.async {
    
     val result = service.getOperations()
     result.map(ops => Ok(Json.obj("status" -> "Ok", "operations" -> Json.toJson(ops))))
  }
  
  def create = Action.async(BodyParsers.parse.json) { request =>
    val operation = request.body.validate[Operation]
    
    operation.fold(
      //Validates the operation object
      errors => Future(BadRequest(Json.obj(
            "status" -> "Parsing operation failed",
            "error" -> JsError.toJson(errors)))),
      operation => {
        val result = service.createOperation(operation)
        result.map(op => Ok(Json.obj("status" -> "Success", "operation" -> Json.toJson(op))))
         .recover{ case thrown => BadRequest(Json.obj("status" -> "Error", "failed" -> "Error while creating the operation. Check the account_id"))}
      })
  }
  
  //Calculates the current balance of a given account (id)
  def get(id: Int) = Action.async {
    
    val result = operationDAO.retrieveOperations(id)
    val balance = result.map(op => service.calculateCurrentBalance(op))
    balance.map(b => Ok(Json.obj("status" -> "Success", "current_balance" -> b)))
  }
  
  //Given an id, init_date and an end_date, finds all statements of it
  def statement(id: Int, init_date: Option[String], end_date: Option[String]) = Action.async {
    (init_date,end_date) match {
      case (None, None) => Future(BadRequest(Json.obj("status" -> "Timeframe is necessary","error" -> "You have to inform init and end date as query param")))
      case (init_date, end_date) => {
        val result = operationDAO.retrieveOperations(id)
        
        val init = DataHandler.stringToDate(init_date.get)
        val end = DataHandler.stringToDate(end_date.get)
          
        result.map(op => Ok(Json.obj("status" -> "Success", "statements" -> Json.toJson(service.generateStatement(init, end, op)))))  
      }
    }
  }
  
  def debts(id: Int) = Action.async {
    
    val result = operationDAO.retrieveOperations(id)
    val debitPeriods = result.map(op => service.loadDebitPeriods(op))
    debitPeriods.map(debits => Ok(Json.obj("status" -> "Success", "debit_periods" -> Json.toJson(debits))))

  }
  
}