package dao;

import scala.concurrent.Future
import java.sql.Date

import slick.driver.JdbcProfile  
import slick.driver.PostgresDriver.api._  
import play.api.db.slick.DatabaseConfigProvider

import javax.inject._

import models.Account
import models.Accounts

/*
  This is a DAO for the Accounts object
 */
trait AccountsDAOTrait {
   def retrieveAllAccounts(): Future[Seq[Account]]
   def createAccount(account: Account): Future[Account]
   def deleteAccount(id: Int): Future[Int]
}

@Singleton
class AccountsDAO @Inject()(dbConfigProvider: DatabaseConfigProvider) extends AccountsDAOTrait {
  
  val dbConfig = dbConfigProvider.get[JdbcProfile]
  
  val accounts = TableQuery[Accounts]
  
  
  override def retrieveAllAccounts(): Future[Seq[Account]] = {
    dbConfig.db.run(accounts.result)
  }
  
  override def createAccount(account: Account): Future[Account] = {
    
    dbConfig.db.run(accounts returning accounts += account)
  }
  
  override def deleteAccount(id: Int): Future[Int] = {

    val query = for {
      account <- accounts if account.id === id
    } yield account
    
    dbConfig.db.run(query.delete)
  }
}