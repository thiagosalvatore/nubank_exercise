package dao;

import scala.concurrent.Future
import java.sql.Date

import slick.driver.JdbcProfile  
import slick.driver.PostgresDriver.api._  
import play.api.db.slick.DatabaseConfigProvider

import javax.inject._

import utils.OperationUtilsTrait
import models.Operation
import models.Operations

trait OperationsDAOTrait {
   def retrieveOperations(accountId : Int) : Future[Seq[(Int, String, Date, String)]]
   def getAllOperations(): Future[Seq[Operation]]
   def createOperation(operation: Operation): Future[Operation]
}

@Singleton
class OperationsDAO @Inject()(dbConfigProvider: DatabaseConfigProvider, OperationUtils: OperationUtilsTrait) extends OperationsDAOTrait {
  
  val dbConfig = dbConfigProvider.get[JdbcProfile]
  
  val operations = TableQuery[Operations]
  /*
    Retrieves all operations from the database
   */
  override def getAllOperations(): Future[Seq[Operation]] = {
    dbConfig.db.run(operations.result)
  }

  // Saves a new operation in the database
  override def createOperation(operation: Operation): Future[Operation] = {
    
    val finalOperation = OperationUtils.negativeAmountForDebitOperation(operation)
    
    dbConfig.db.run(operations returning operations += finalOperation)
  }
  /*
   * Given an account, fetch all operations related to it from the database, and generates
   * a tuple with the amount, kind and date of the operation
   */
  def retrieveOperations(accountId : Int) : Future[Seq[(Int, String, Date, String)]] = {
    
    val query = (for {
      op <- operations if op.account_id === accountId
    } yield (op.amount, op.kind, op.date, op.description))
    
    dbConfig.db.run(query.result)
  }
}
