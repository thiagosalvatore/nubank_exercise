package models;

import play.api.libs.json._  
import play.api.libs.functional.syntax._
import play.api.libs.json._

import org.joda.time.DateTime
import java.sql.Date
import java.text.SimpleDateFormat

case class Operation (
   id: Option[Int],
   account_id: Int,
   amount: Int,
   kind: String,
   description: String,
   date: Date
)

object Operation {
  
  def timestampToDateTime(t: Date): String = new SimpleDateFormat("yyyy-MM-dd").format(t)
  
  def dateTimeToTimestamp(dt: DateTime): Date = new Date(dt.getMillis)
  
  implicit val timestampFormat = new Format[Date] {
  
      def writes(t: Date): JsValue = Json.toJson(timestampToDateTime(t))
  
      def reads(json: JsValue): JsResult[Date] = Json.fromJson[DateTime](json).map(dateTimeToTimestamp)
  
    }
  
  implicit val operationReads: Reads[Operation] = (
    (JsPath \ "id").readNullable[Int] and
    (JsPath \ "account_id").read[Int] and
    (JsPath \ "amount").read[Int] and
    (JsPath \ "kind").read[String] and
    (JsPath \ "description").read[String] and
    (JsPath \ "date").read[Date]
  )(Operation.apply _)


  implicit val operationWrites: Writes[Operation] = (
    (JsPath \ "id").writeNullable[Int] and
    (JsPath \ "account_id").write[Int] and
    (JsPath \ "amount").write[Int] and
    (JsPath \ "kind").write[String] and
    (JsPath \ "description").write[String] and
    (JsPath \ "date").write[Date]
  )(unlift(Operation.unapply _))
}