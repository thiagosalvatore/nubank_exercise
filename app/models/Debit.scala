package models;

import play.api.libs.json._  
import play.api.libs.functional.syntax._
import play.api.libs.json._

import org.joda.time.DateTime
import java.sql.Date
import java.text.SimpleDateFormat

case class Debit (
   principal: Int,
   start: Date,
   end: Option[Date]
)

object Debit {
  
  def timestampToDateTime(t: Date): String = new SimpleDateFormat("yyyy-MM-dd").format(t)
  
  def dateTimeToTimestamp(dt: DateTime): Date = new Date(dt.getMillis)
  
  implicit val timestampFormat = new Format[Date] {
  
      def writes(t: Date): JsValue = Json.toJson(timestampToDateTime(t))
  
      def reads(json: JsValue): JsResult[Date] = Json.fromJson[DateTime](json).map(dateTimeToTimestamp)
  
    }
  
  implicit val debitReads: Reads[Debit] = (
    (JsPath \ "principal").read[Int] and
    (JsPath \ "start").read[Date] and
    (JsPath \ "end").readNullable[Date]
  )(Debit.apply _)


  implicit val debitWrites: Writes[Debit] = (
    (JsPath \ "principal").write[Int] and
    (JsPath \ "start").write[Date] and
    (JsPath \ "end").writeNullable[Date]
  )(unlift(Debit.unapply _))
}