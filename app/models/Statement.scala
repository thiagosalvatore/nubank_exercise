package models;

import play.api.libs.json._  
import play.api.libs.functional.syntax._
import play.api.libs.json._

import org.joda.time.DateTime
import java.sql.Date
import java.text.SimpleDateFormat

case class Statement (
   amount: Int,
   kind: String,
   date: Date,
   description: String
)


object Statement {
  
  def timestampToDateTime(t: Date): String = new SimpleDateFormat("yyyy-MM-dd").format(t)
  
  def dateTimeToTimestamp(dt: DateTime): Date = new Date(dt.getMillis)
  
  implicit val timestampFormat = new Format[Date] {
  
      def writes(t: Date): JsValue = Json.toJson(timestampToDateTime(t))
  
      def reads(json: JsValue): JsResult[Date] = Json.fromJson[DateTime](json).map(dateTimeToTimestamp)
  
    }
  
  implicit val statementReads: Reads[Statement] = (
    (JsPath \ "amount").read[Int] and
    (JsPath \ "kind").read[String] and
    (JsPath \ "date").read[Date] and
    (JsPath \ "description").read[String]
  )(Statement.apply _)


  implicit val statementWrites: Writes[Statement] = (
    (JsPath \ "amount").write[Int] and
    (JsPath \ "kind").write[String] and
    (JsPath \ "date").write[Date] and
    (JsPath \ "description").write[String]
  )(unlift(Statement.unapply _))
}