package models;

import slick.driver.JdbcProfile  
import slick.driver.PostgresDriver.api._  
import play.api.db.slick.DatabaseConfigProvider  
import play.api._

import java.sql.Date

class Operations (tag: Tag) extends Table[Operation](tag, "operations") {  
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def account_id = column[Int]("account_id")
  def amount = column[Int]("amount")
  def kind = column[String]("kind")
  def description = column[String]("description")
  def date = column[Date]("date")

  def * = (id.?, account_id, amount, kind, description, date) <> ((Operation.apply _).tupled, Operation.unapply _)
}