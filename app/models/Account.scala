package models;

import play.api.libs.json._  
import play.api.libs.functional.syntax._
import play.api.libs.json._

import org.joda.time.DateTime
import java.sql.Date
import java.text.SimpleDateFormat

case class Account (
   id: Option[Int],
   first_name: String,
   last_name: String
)

object Account {
  
  def timestampToDateTime(t: Date): String = new SimpleDateFormat("yyyy-MM-dd").format(t)
  
  def dateTimeToTimestamp(dt: DateTime): Date = new Date(dt.getMillis)
  
  implicit val timestampFormat = new Format[Date] {
  
      def writes(t: Date): JsValue = Json.toJson(timestampToDateTime(t))
  
      def reads(json: JsValue): JsResult[Date] = Json.fromJson[DateTime](json).map(dateTimeToTimestamp)
  
    }
  
  implicit val accountReads: Reads[Account] = (
    (JsPath \ "id").readNullable[Int] and
    (JsPath \ "first_name").read[String] and
    (JsPath \ "last_name").read[String]
  )(Account.apply _)


  implicit val accountWrites: Writes[Account] = (
    (JsPath \ "id").writeNullable[Int] and
    (JsPath \ "first_name").write[String] and
    (JsPath \ "last_name").write[String]
  )(unlift(Account.unapply _))
}