package models;

import play.api.libs.json._  
import play.api.libs.functional.syntax._
import play.api.libs.json._

import org.joda.time.DateTime
import java.sql.Date
import java.text.SimpleDateFormat

case class Statements (
   balance: Int,
   date: String,
   statements: Seq[Statement]
)


object Statements {
  
  def timestampToDateTime(t: Date): String = new SimpleDateFormat("yyyy-MM-dd").format(t)
  
  def dateTimeToTimestamp(dt: DateTime): Date = new Date(dt.getMillis)
  
  implicit val timestampFormat = new Format[Date] {
  
      def writes(t: Date): JsValue = Json.toJson(timestampToDateTime(t))
  
      def reads(json: JsValue): JsResult[Date] = Json.fromJson[DateTime](json).map(dateTimeToTimestamp)
  
    }
  
  implicit val statemenstReads: Reads[Statements] = (
    (JsPath \ "balance").read[Int] and
    (JsPath \ "date").read[String] and
    (JsPath \ "statements").read[Seq[Statement]]
  )(Statements.apply _)


  implicit val statementsWrites: Writes[Statements] = (
    (JsPath \ "balance").write[Int] and
    (JsPath \ "date").write[String] and
    (JsPath \ "statements").write[Seq[Statement]]
  )(unlift(Statements.unapply _))
}