package models;

import slick.driver.JdbcProfile  
import slick.driver.PostgresDriver.api._  
import play.api.db.slick.DatabaseConfigProvider  
import play.api._

import java.sql.Date


class Accounts (tag: Tag) extends Table[Account](tag, "accounts") {  
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def first_name = column[String]("first_name")
  def last_name = column[String]("last_name")

  def * = (id.?, first_name, last_name) <> ((Account.apply _).tupled, Account.unapply _)
}