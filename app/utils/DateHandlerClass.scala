package utils;

import javax.inject._

import java.sql.Date
import org.joda.time.DateTime
import java.text.SimpleDateFormat

/**
 * This trait demonstrates how to create a component that is injected
 * into a controller. The trait represents a counter that returns a
 * incremented number each time it is called.
 */
trait DataHandler {
  def isAfterToday(date: Date): Boolean
  def isBetweenDates(begin: Option[Date], end: Date, date: Date): Boolean
  def sqlDateToString(date: Date): String
  def sqlDateToDatetime(date: Date): DateTime
  def stringToDatetime(date: String) : DateTime
  def oneDayBefore(date: Date) : Date
  def stringToDate(date: String) : Date
}

@Singleton
class DataHandlerClass extends DataHandler {
  
  val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
  
  override def isAfterToday(date: Date): Boolean = {
    
    val now = DateTime.now()
    val other = new DateTime(date)
    
    if(now.isBefore(other)) {
      true
    }else{
      false
    }
  }
  
  override def sqlDateToString(date: Date): String = {
    dateFormat.format(date)
  }
  
  override def sqlDateToDatetime(date: Date): DateTime = {
    new DateTime(dateFormat.format(date))
  }
  
  override def stringToDatetime(date: String) : DateTime = {
    new DateTime(dateFormat.parse(date))
  }
  
  override def stringToDate(date: String) : Date = {
    new Date(DateTime.parse(date).getMillis)
  }
  
  override def oneDayBefore(date: Date) : Date = {
    
    val sf = sqlDateToDatetime(date)
    new Date(sf.plusDays(-1).getMillis)
  }
  
  override def isBetweenDates(begin: Option[Date], end: Date, date: Date): Boolean = {
   
    val beginDate = begin.getOrElse(None)
    val finish = new DateTime(end)
    val to = new DateTime(date)
    
    if(begin == None){
      if(!to.isAfter(finish)){
        return true
      }else{
        return false
      }
    }else{
      val init = new DateTime(beginDate)
      if(!to.isBefore(init) && !to.isAfter(finish)) {
        true
      }else{
        false
      } 
    }
  }
}