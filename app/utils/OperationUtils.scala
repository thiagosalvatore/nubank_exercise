package utils;

import javax.inject._

import java.sql.Date
import org.joda.time.DateTime

import models.Operation
import models.Statement
import models.Statements

trait OperationUtilsTrait {
  def filterOutFutureOperations(operations: Seq[(Int, String, Date, String)]): Seq[(Int, String, Date, String)]
  def isCreditOperation(operation: Operation) : Boolean
  def negativeAmountForDebitOperation(operation: Operation) : Operation
  def filterOperationsBetweenDates(init: Option[Date], end: Date, operations: Seq[(Int, String, Date, String)]) : Seq[(Int, String, Date, String)]
  def calculateBalanceUntilDate(end: Date, operations: Seq[(Int, String, Date, String)]) : Int
  def groupOperationsByDate(sequence: Seq[(Int, String, Date, String)]) : Iterable[models.Statements]
  def sumOperationsAmount(operations: Seq[(Int, String, Date, String)]) : Int
}

@Singleton
class OperationUtils @Inject()(dataHandler: DataHandler, StatementUtils: StatementsUtilsTrait) extends OperationUtilsTrait {
  
  //List of kinds that indicates incrase on balance
  val creditKind = List("deposit", "salary", "credit")
  
  //List of kinds that indicates decrease on balance
  val debitKind = List("purchase", "withdraw", "debit")
  
  override def filterOutFutureOperations(operations: Seq[(Int, String, Date, String)]): Seq[(Int, String, Date, String)] = {
    operations.filter { operation => !dataHandler.isAfterToday(operation._3) }
  }
  
  override def sumOperationsAmount(operations: Seq[(Int, String, Date, String)]) : Int = {
    operations.map(_._1).sum
  }
 
  override def calculateBalanceUntilDate(end: Date, operations: Seq[(Int, String, Date, String)]) : Int = {
    
    val operationsUntilEndDate = filterOperationsBetweenDates(end = end, operations = operations)
    sumOperationsAmount(operationsUntilEndDate)
  }
  
  def isCreditOperation(operation: Operation) : Boolean = {
    creditKind.contains(operation.kind)
  }
  
  def negative(number: Int) : Int = {
    0 - number
  }
  
  override def negativeAmountForDebitOperation(operation: Operation) : Operation = {
    
    val isCredit = isCreditOperation(operation)
    
    if (!isCredit) Operation(operation.id, operation.account_id, negative(operation.amount), operation.kind, operation.description, operation.date) else operation
  }
  
  override def filterOperationsBetweenDates(init: Option[Date] = None, end: Date, operations: Seq[(Int, String, Date, String)]) : Seq[(Int, String, Date, String)] = {
    operations.filter { obj => dataHandler.isBetweenDates(init, end, obj._3) }
  }
  
  def groupByDate(sequence: Seq[(Int, String, Date, String)]): Map[String, Seq[(Int, String, Date, String)]] = {
    sequence.groupBy{ p => dataHandler.sqlDateToString(p._3)} 
  }
  
  
  //Group a sequence of (Amount, Description, Date) by date, generating an Map of Date -> Statements on this date
  override def groupOperationsByDate(sequence: Seq[(Int, String, Date, String)]) : Iterable[models.Statements] = {

    val dateStatementsGrouped = groupByDate(sequence)
    
    StatementUtils.tupleDateStatementsToStatements(dateStatementsGrouped)
   }

}