package utils;

import javax.inject._

import java.sql.Date
import org.joda.time.DateTime

import models.Statement
import models.Statements

trait StatementsUtilsTrait {
  
  def sumStatementsAmount(sequence: Seq[Statement], initialValue: Int) : Int
  def createStatement(amount: Int, kind: String, date: Date, description: String): Statement
  def createSeqOfStatement(statements : Seq[(Int,String,Date,String)]) : Seq[Statement]
  def tupleDateStatementsToStatements(dateStatements : Map[String, Seq[(Int, String, Date, String)]]) : Iterable[Statements]
  def acumulateBalance(statements: Seq[Statements], initialValue: Int) : Seq[Statements]
  def sortStatementsByDate(statements: Iterable[Statements]) : Seq[Statements]
}

@Singleton
class StatementUtils @Inject()(dataHandler: DataHandler) extends StatementsUtilsTrait {
  
  override def sumStatementsAmount(sequence: Seq[Statement], initialValue: Int) : Int = {
    initialValue + sequence.map(_.amount).sum
  }
  
  override def createStatement(amount: Int, kind: String, date: Date, description: String): Statement = {
    Statement(amount, kind, date, description)
  }
  
  override def createSeqOfStatement(statements : Seq[(Int,String,Date,String)]) : Seq[Statement] = {
     statements.map( s => createStatement(s._1, s._2, s._3, s._4))
  }
  
  override def tupleDateStatementsToStatements(dateStatement : Map[String, Seq[(Int, String, Date, String)]]) : Iterable[Statements] = {
    val statements = mapDateStatementToStatements(dateStatement)
    iterableToSeq(statements)
  }
  
  override def acumulateBalance(statements: Seq[Statements], initialValue: Int) : Seq[Statements] = {
   val statementsBalance = mapStatementsToBalance(statements)
   val acumulatedSum = sumBalancesKeepingSeq(statementsBalance, initialValue)
   val pairBalanceStatements = concatBalanceStatements(acumulatedSum, statements)
   mapPairBalanceStatementsToStatements(pairBalanceStatements)
  }
  
  
  override def sortStatementsByDate(statements: Iterable[Statements]) : Seq[Statements] = {
    val seqStatements = iterableToSeq(statements)
    sortStatementsByDate(seqStatements)
  }
  
  // ------------------------------ Auxiliar functions ---------------------------------------- //
  
  def getStatementBalance(statements: Statements) : Int = {
    statements.balance
  }
  
  def mapStatementsToBalance(statements: Seq[Statements]) : Seq[Int] = {
    statements.map(s => getStatementBalance(s))
  }
  
  def sumBalancesKeepingSeq(balances: Seq[Int], initialValue: Int) : Seq[Int] = {
    val balancesSeq = balances.scanLeft(initialValue)(_+_)
    removeFirst(balancesSeq)
  }
  
  def removeFirst(seq: Seq[Int]): Seq[Int] = {
    seq.drop(1)
  }
  
  def concatBalanceStatements(balances: Seq[Int], statements: Seq[Statements]) : Seq[(Int, Statements)] = {
    balances.zip(statements)
  }
  
  def mapPairBalanceStatementsToStatements(balanceStatements: Seq[(Int, Statements)]) : Seq[Statements] = {
    balanceStatements.map{ case (balance, statements) => Statements(balance, statements.date, statements.statements)}
  }
  
  def iterableToSeq(statements: Iterable[Statements]) : Seq[Statements] = {
    statements.toSeq
  }
  
  def createStatements(statements:  Seq[(Int, String, Date, String)], date: String) : Statements = {
    val seqOfStatement = createSeqOfStatement(statements)
    val totalBalance = sumStatementsAmount(seqOfStatement, 0)
    Statements(totalBalance, date, seqOfStatement)
  }
  
  def mapDateStatementToStatements(dateStatements : Map[String, Seq[(Int, String, Date, String)]]):  Iterable[Statements] = {
    dateStatements.map {case (date, statements) => createStatements(statements, date) }
  }
  
  def compareStatementsDates(s1: Statements, s2: Statements) = {
    
    val date1 = dataHandler.stringToDatetime(s1.date)
    val date2 = dataHandler.stringToDatetime(s2.date)
    date1.isBefore(date2)
  }
  
  def sortStatementsByDate(statements: Seq[Statements]) = {
    statements.sortWith(compareStatementsDates)
  }
  
}