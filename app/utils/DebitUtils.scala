package utils;

import javax.inject._

import java.sql.Date
import org.joda.time.DateTime

import models.Statement
import models.Statements
import models.Debit

trait DebitUtilsTrait {
   def findDebitPeriods(statements: Seq[Statements]) : Seq[Debit]
}

@Singleton
class DebitUtils @Inject()(dataHandler: DataHandler) extends DebitUtilsTrait {
  
  override def findDebitPeriods(statements: Seq[Statements]) : Seq[Debit] = {
    
    val statementsAsDebit = statementsToSeqDebit(statements)
    
    val debitList = generateDebitList(statementsAsDebit)
    
    filterNegativePrincipal(debitList)
    
  }
    
  def isSizeOne(debits: Seq[Debit]) : Boolean = {
      debits.size < 2
  }
    
  def debitToSeqDebit(debit: Debit) : Seq[Debit] = {
    Seq(debit)
  }
  
  def newDebitEndDate(debit: Debit, endDate: Option[Date]) : Debit = {
    
    Debit(debit.principal, debit.start,  endDate)
    
  }
  
  def isLastDebit(debits: Seq[Debit], current: Debit) : Boolean = {
    current.equals(debits.last)
  }
  
  def addDebitToList(debit: Debit, list: List[Debit]) : List[Debit] = {
     list :+ debit
  }
  
  def generateEndDate(debit: Option[Debit]) : Option[Date] = {
    debit match {
      case None => None
      case Some(debit) => {
        val endsBeforeNext = dataHandler.oneDayBefore(debit.start)
        Option(endsBeforeNext)    
      }
    }
  }
  
  def createAndInsertDebit(debit1: Debit, debit2: Option[Debit], list: List[Debit]) : List[Debit] = {
    val endDate = generateEndDate(debit2)
    val debitObj = newDebitEndDate(debit1, endDate)
    addDebitToList(debitObj, list)
  }
  
  def generatePairOfDebits(debits: Seq[Debit]) : Iterator[Seq[Debit]] = {
    debits.sliding(2)
  }
    
  def generateDebitList(debits: Seq[Debit]) : Seq[Debit] = {
    
    var debitsList = List.empty[Debit]
    
    val sizeListOne = isSizeOne(debits)
    
    //Sliding wouldn't work for a list with size smaller than 2, so, there is a need for this checking
    if(sizeListOne){
      debits.foreach{ case debit => debitsList = createAndInsertDebit(debit, None, debitsList)}
    }else{
      val pairs = generatePairOfDebits(debits)
      
      pairs.foreach{ case Seq(debit1, debit2) => {
        
        debitsList = createAndInsertDebit(debit1, Option(debit2), debitsList)
   
        val isLast = isLastDebit(debits, debit2)
        
        if(isLast) debitsList = createAndInsertDebit(debit2, None, debitsList)
  
      }} 
    }
    debitsList
  }
  
  def statementsToDebit(s: Statements) : Debit = {
    val strToDate = dataHandler.stringToDate(s.date)
    Debit(s.balance, strToDate, None)
  }
  
  def mapSeqStatementsToSeqDebit(statements: Seq[Statements]) : Seq[Debit] = {
    statements.map(statementsToDebit)
  }

  def statementsToSeqDebit(statements: Seq[Statements]) : Seq[Debit] = {
    mapSeqStatementsToSeqDebit(statements)
  }
  
  def isPrincipalNegative(debit: Debit) : Boolean = {
    debit.principal < 0
  }
  
  def filterNegativePrincipal(debitList: Seq[Debit]) : Seq[Debit] = {
    debitList.filter(isPrincipalNegative)
  }
  
}