package services;

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.concurrent.ExecutionContext.Implicits.global  

import models.Operation
import models.Statement
import models.Statements
import models.Operations
import models.Debit

import dao.OperationsDAOTrait

import utils.DataHandler
import utils.OperationUtilsTrait
import utils.StatementsUtilsTrait
import utils.DebitUtilsTrait

import java.text.SimpleDateFormat
import java.sql.Date
import org.joda.time.DateTime
import scala.collection.mutable.ListBuffer

import javax.inject._


trait OperationTrait {
  def calculateCurrentBalance(operations: Seq[(Int, String, Date, String)]): Int
  def generateStatement(init: Date, end: Date, operations: Seq[(Int, String, Date, String)]): Iterable[Statements]
  def loadDebitPeriods(operations: Seq[(Int, String, Date, String)]) : Seq[Debit]
  def getOperations() : Future[Seq[Operation]]
  def createOperation(operation: Operation) : Future[Operation]
}

@Singleton
class OperationService @Inject()(OperationsDAO : OperationsDAOTrait, dataHandler: DataHandler, OperationUtils: OperationUtilsTrait, StatementUtils: StatementsUtilsTrait, DebitUtils: DebitUtilsTrait) extends OperationTrait {
  
  override def getOperations() : Future[Seq[Operation]] = {
    OperationsDAO.getAllOperations()
  }
  
  override def createOperation(operation: Operation) : Future[Operation] = {
    OperationsDAO.createOperation(operation)
  }
  
  override def calculateCurrentBalance(operations: Seq[(Int, String, Date, String)]): Int = {
    
    val pastPresentOperations = OperationUtils.filterOutFutureOperations(operations)
    OperationUtils.sumOperationsAmount(pastPresentOperations)
    
  }
  
  override def loadDebitPeriods(operations: Seq[(Int, String, Date, String)]) : Seq[Debit] = {
    
    val statementsByDate = OperationUtils.groupOperationsByDate(operations)
    
    val statementsSorted = StatementUtils.sortStatementsByDate(statementsByDate)
 
    val statementsAcumulated = StatementUtils.acumulateBalance(statementsSorted, 0)
    
    DebitUtils.findDebitPeriods(statementsAcumulated)
    
  }
  
  //Generate a mapping of statements by date filtered according to a initial date and final date
  override def generateStatement(init: Date, end: Date, operations: Seq[(Int, String, Date, String)]): Iterable[models.Statements] = {

    val dateLimitBalance = dataHandler.oneDayBefore(init)
    
    //Calculates the balance of the account until one day before the timeframe defined for the statement
    val balanceUntilDate = OperationUtils.calculateBalanceUntilDate(dateLimitBalance, operations)
    
    val operationsWithinTimeframe = OperationUtils.filterOperationsBetweenDates(Option(init), end, operations)

    //Group the operations according to date
    val statementsByDate = OperationUtils.groupOperationsByDate(operationsWithinTimeframe)
    
    //Sort the operations by oldest
    val statementsSorted = StatementUtils.sortStatementsByDate(statementsByDate)
    
    //Stores the balance until one day before the timeframe, and acumulate the next balances based on this
    StatementUtils.acumulateBalance(statementsSorted, balanceUntilDate)
  }
 
}
