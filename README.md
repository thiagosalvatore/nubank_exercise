# Nubank Exercise - Bank System

## Pre-requisites
- Scala - https://www.scala-lang.org/download/
- PostgreSQL 9.5 - https://www.postgresql.org/
- Java 8

This solution comes with a pre-packaged version of sbt, so, you don't have to worry about download and installing it

## Frameworks Used
- Play Framework for Testing and HTTP mapping - https://www.playframework.com/

## How to run

First of all, you have to have a database created on your PostgreSQL. This app is configured to use the following settings for the database:

Name: nubank
Host: localhost
User: postgres
Password: postgres

If you wish to change that, you can do by opening conf/application.conf, and searching for:

slick.dbs.default.db.url="jdbc:postgresql://localhost/nubank"  
slick.dbs.default.db.user="postgres"  
slick.dbs.default.db.password="postgres"

Then you can run the commands below

```sh
$ git clone https://thiagosalvatore@bitbucket.org/thiagosalvatore/nubank_exercise.git
$ cd nubank_exercise
$ sbt run
```

If you face any problem when trying to run ```sbt run```, check if it has running permissions.

Then, go to http://localhost:9000/api/accounts, and execute the Evolution Script to update the database schema.

## How to build dist version
```sh
$ git clone https://thiagosalvatore@bitbucket.org/thiagosalvatore/nubank_exercise.git
$ cd nubank_exercise
$ sbt dist
```
Then, you will find the dist version on target/universal.

## How to run tests
```sh
$ git clone https://thiagosalvatore@bitbucket.org/thiagosalvatore/nubank_exercise.git
$ cd nubank_exercise
$ sbt test
```

The application will be running at http://localhost:9000

## Structure
This challenge is structured following the MVC pattern (in this case, there are no Views):

- app/controllers - Stores all controllers (receive a request from a URL, and map it to a specific Service)
- app/models - Stores all models, serializers and DAO's
- app/services - Do all the business logic, and returns the result to the controllers
- app/utils - Helper classes
- conf/evolutions - Used to recreate the database, upgrade and downgrade it (when your first run the application, it will run all evolutions to create the database schema)
- conf/routes - Store all routes and its respective controllers


## Endpoints

- The documentation for all endpoints can be found at http://docs.nubank.apiary.io