import javax.inject._

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future

import dao.OperationsDAO

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ApplicationSpec extends PlaySpec with OneAppPerTest {
  
  def create_operation(account_id : Int, amount: Int, kind: String, date : String) : Future[Result] = {
    val obj = Json.parse("""{ "account_id": """ + account_id + """, "amount" : """ + amount + """, "description" : "Test", "kind" : """" + kind + """", "date" : """"+date+"""" }""")
   
    route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get
  }
  
  def create_account(first_name : String, last_name : String) : Future[Result] = {
      val obj = Json.parse("""{ "first_name": """" + first_name + """", "last_name" : """" + last_name + """" }""")
      
      route(app, FakeRequest(POST, "/api/accounts").withJsonBody(obj)).get
  }
  
  def delete_account(id: Int): Future[Result] = {
    route(app, FakeRequest(DELETE, "/api/accounts/" + id)).get
  }

  "Routes" should {

    "send 404 on a bad request" in  {
      route(app, FakeRequest(GET, "/boum")).map(status(_)) mustBe Some(NOT_FOUND)
    }

  }

  "OperationController#POST-/api/operations" should {
    
    "fail when creating operation because account_id is not a number" in {
      val obj = Json.parse("""{ "account_id": "1", "amount" : 1500, "description" : "Credit card payment", "kind" : "payment", "date" : "2017-03-21" }""")
      val operation = route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get

      status(operation) mustBe 400
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      code mustBe "Parsing operation failed"
    }

    "fail when creating operation because account_id is missing" in {
      val obj = Json.parse("""{ "amount" : 1500, "description" : "Credit card payment", "kind" : "payment", "date" : "2017-03-21" }""")
      val operation = route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get

      status(operation) mustBe 400
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      code mustBe "Parsing operation failed"
    }
    
    "fail when creating operation because account_id is doesn't exist" in {
      val obj = Json.parse("""{ "account_id" : 100, "amount" : 1500, "description" : "Credit card payment", "kind" : "payment", "date" : "2017-03-21" }""")
      val operation = route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get

      status(operation) mustBe 400
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      val error = (result \ "failed").as[String]
      code mustBe "Error"
      error mustBe "Error while creating the operation. Check the account_id"
    }
    
    "fail when creating operation because amount is missing" in {
      val obj = Json.parse("""{"account_id": 1, "description" : "Credit card payment", "kind" : "payment", "date" : "2017-03-21" }""")
      val operation = route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get

      status(operation) mustBe 400
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      code mustBe "Parsing operation failed"
    }
    
    "fail when creating operation because description is missing" in {
      val obj = Json.parse("""{"account_id": 1, "amount": 1500, "kind" : "payment", "date" : "2017-03-21" }""")
      val operation = route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get

      status(operation) mustBe 400
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      code mustBe "Parsing operation failed"
    }
    
    "fail when creating operation because kind is missing" in {
      val obj = Json.parse("""{"account_id": 1, "amount": 1500, "description" : "Credit card payment", "date" : "2017-03-21" }""")
      val operation = route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get

      status(operation) mustBe 400
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      code mustBe "Parsing operation failed"
    }
    
    "fail when creating operation because date is missing" in {
      val obj = Json.parse("""{"account_id": 1, "amount": 1500, "description" : "Credit card payment", "kind" : "payment"}""")
      val operation = route(app, FakeRequest(POST, "/api/operations").withJsonBody(obj)).get

      status(operation) mustBe 400
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      code mustBe "Parsing operation failed"
    }
    
    "create a operation" in {

      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      
      val operation = create_operation(id, 1500, "payment", "2017-03-21")

      status(operation) mustBe OK
      contentType(operation) mustBe Some("application/json")
      
      val result = contentAsJson(operation).as[JsObject]
      
      val code = (result \ "status").as[String]
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      code mustBe "Success"
    }

  }
  
  "OperationController#GET-/api/balance" should {
    "return the balance of a new account equal to 0" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val get_balance = route(app, FakeRequest(GET, "/api/balance/" + id)).get
      
      status(get_balance) mustBe OK
      contentType(get_balance) mustBe Some("application/json")
      
      val result_balance = contentAsJson(get_balance).as[JsObject]
      
      val balance = (result_balance \ "current_balance").as[Int]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      balance mustBe 0
    }
    
    "calculate the correct balance after 3 deposits" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "deposit", "2017-03-18")
      val operation2 = create_operation(id, 1500, "deposit", "2017-03-18")
      val operation3 = create_operation(id, 1500, "deposit", "2017-03-18")

      status(operation1) mustBe OK
      status(operation2) mustBe OK
      status(operation3) mustBe OK
      
      val get_balance = route(app, FakeRequest(GET, "/api/balance/" + id)).get
      
      status(get_balance) mustBe OK
      contentType(get_balance) mustBe Some("application/json")
      
      val result_balance = contentAsJson(get_balance).as[JsObject]
      
      val balance = (result_balance \ "current_balance").as[Int]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      balance mustBe 4500
      
    }
    
    "calculate the correct balance after 3 purchases" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "purchase", "2017-03-18")
      val operation2 = create_operation(id, 1500, "purchase", "2017-03-18")
      val operation3 = create_operation(id, 1500, "purchase", "2017-03-18")

      status(operation1) mustBe OK
      status(operation2) mustBe OK
      status(operation3) mustBe OK
      
      val get_balance = route(app, FakeRequest(GET, "/api/balance/" + id)).get
      
      status(get_balance) mustBe OK
      contentType(get_balance) mustBe Some("application/json")
      
      val result_balance = contentAsJson(get_balance).as[JsObject]
      
      val balance = (result_balance \ "current_balance").as[Int]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      balance mustBe -4500
      
    }
    
    "calculate the correct balance after 3 purchases and 2 deposits" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "deposit", "2017-03-18")
      val operation2 = create_operation(id, 400, "purchase", "2017-03-19")
      val operation3 = create_operation(id, 400, "purchase", "2017-03-19")
      val operation4 = create_operation(id, 400, "purchase", "2017-03-19")
      val operation5 = create_operation(id, 1500, "deposit", "2017-03-18")

      status(operation1) mustBe OK
      status(operation2) mustBe OK
      status(operation3) mustBe OK
      status(operation4) mustBe OK
      status(operation5) mustBe OK
      
      val get_balance = route(app, FakeRequest(GET, "/api/balance/" + id)).get
      
      status(get_balance) mustBe OK
      contentType(get_balance) mustBe Some("application/json")
      
      val result_balance = contentAsJson(get_balance).as[JsObject]
      
      val balance = (result_balance \ "current_balance").as[Int]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      balance mustBe 1800
      
    }
    
    "calculate the correct balance after 3 purchases and 2 deposits (one future deposit, not yet added to the balance)" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "deposit", "2017-03-18")
      val operation2 = create_operation(id, 400, "purchase", "2017-03-19")
      val operation3 = create_operation(id, 400, "purchase", "2017-03-19")
      val operation4 = create_operation(id, 400, "purchase", "2017-03-19")
      val operation5 = create_operation(id, 1500, "deposit", "2019-03-18")

      status(operation1) mustBe OK
      status(operation2) mustBe OK
      status(operation3) mustBe OK
      status(operation4) mustBe OK
      status(operation5) mustBe OK
      
      val get_balance = route(app, FakeRequest(GET, "/api/balance/" + id)).get
      
      status(get_balance) mustBe OK
      contentType(get_balance) mustBe Some("application/json")
      
      val result_balance = contentAsJson(get_balance).as[JsObject]
      
      val balance = (result_balance \ "current_balance").as[Int]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      balance mustBe 300
      
    }
    
  }

  
  "OperationController#GET-/api/statements" should {
 
    "return the statements for one day only with balance equal to 1100" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "deposit", "2017-03-19")
      val operation2 = create_operation(id, 1500, "deposit", "2019-03-21")
      val operation3 = create_operation(id, 400, "purchase", "2017-03-19")

      status(operation1) mustBe OK
      status(operation2) mustBe OK
      status(operation3) mustBe OK
            
      val get_statements = route(app, FakeRequest(GET, "/api/statements/" + id + "?init=2017-03-19&end=2017-03-19")).get
      
      status(get_statements) mustBe OK
      contentType(get_statements) mustBe Some("application/json")
      
      val result_statements = contentAsJson(get_statements).as[JsObject]
      
      val statements = (result_statements \ "statements").as[Seq[JsObject]]
      
      val balance = (statements(0) \ "balance").as[Int]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      balance mustBe 1100
      
    }
    
    "return the statements for one day only with balance equal to 2600 but with a previous balance of 1100" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "deposit", "2017-03-19")
      val operation2 = create_operation(id, 1500, "deposit", "2017-03-21")
      val operation3 = create_operation(id, 400, "purchase", "2017-03-19")
 
      status(operation1) mustBe OK
      status(operation2) mustBe OK
      status(operation3) mustBe OK
            
      val get_statements = route(app, FakeRequest(GET, "/api/statements/" + id + "?init=2017-03-21&end=2017-03-21")).get
      
      status(get_statements) mustBe OK
      contentType(get_statements) mustBe Some("application/json")
      
      val result_statements = contentAsJson(get_statements).as[JsObject]
      
      val statements = (result_statements \ "statements").as[Seq[JsObject]]
      
      val balance = (statements(0) \ "balance").as[Int]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      balance mustBe 2600
      
    }
    
    "return no statements" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
            
      val get_statements = route(app, FakeRequest(GET, "/api/statements/" + id + "?init=2017-03-21&end=2017-03-21")).get
      
      status(get_statements) mustBe OK
      contentType(get_statements) mustBe Some("application/json")
      
      val result_statements = contentAsJson(get_statements).as[JsObject]
      
      val statements = (result_statements \ "statements").as[Seq[JsObject]]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      statements.size mustBe 0
      
    }
    
  }
  
  "OperationController#GET-/api/debits" should {
    "return that the current's account balance is negative" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "deposit", "2017-03-19")
      val operation2 = create_operation(id, 2000, "purchase", "2017-03-19")

      status(operation1) mustBe OK
      status(operation2) mustBe OK
            
      val get_debits = route(app, FakeRequest(GET, "/api/debits/" + id)).get
      
      status(get_debits) mustBe OK
      contentType(get_debits) mustBe Some("application/json")
      
      val result_debits = contentAsJson(get_debits).as[JsObject]
      
      val debits = (result_debits \ "debit_periods").as[Seq[JsObject]]
      
      val principal = (debits(0) \ "principal").as[Int]
      val start = (debits(0) \ "start").as[String]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      principal mustBe -500
      start mustBe "2017-03-19"
      
    }
    
    "return that the balance was negative (-1000) from 2017-03-15 until 2017-03-18" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
      
      val operation1 = create_operation(id, 1500, "deposit", "2017-03-19")
      
      status(operation1) mustBe OK
      val operation2 = create_operation(id, 1000, "purchase", "2017-03-15")

      status(operation2) mustBe OK
            
      val get_debits = route(app, FakeRequest(GET, "/api/debits/" + id)).get
      
      status(get_debits) mustBe OK
      contentType(get_debits) mustBe Some("application/json")
      
      val result_debits = contentAsJson(get_debits).as[JsObject]
      
      val debits = (result_debits \ "debit_periods").as[Seq[JsObject]]
      
      val principal = (debits(0) \ "principal").as[Int]
      val start = (debits(0) \ "start").as[String]
      val end =  (debits(0) \ "end").as[String]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      principal mustBe -1000
      start mustBe "2017-03-15"
      end mustBe "2017-03-18"
      
    }
    
    "return that the balance was never negative" in {
      
      val account = create_account("Conta", "Teste")
      
      status(account) mustBe OK
      contentType(account) mustBe Some("application/json")
      
      val result_account = contentAsJson(account).as[JsObject]
      
      val acc = (result_account \ "account").as[JsObject]
      
      val id = (acc \ "id").as[Int]
            
      val get_debits = route(app, FakeRequest(GET, "/api/debits/" + id)).get
      
      status(get_debits) mustBe OK
      contentType(get_debits) mustBe Some("application/json")
      
      val result_debits = contentAsJson(get_debits).as[JsObject]
      
      val debits = (result_debits \ "debit_periods").as[Seq[JsObject]]
      
      val del = delete_account(id)
      
      status(del) mustBe OK
      
      debits.size mustBe 0
      
      
    }
    
  }
}